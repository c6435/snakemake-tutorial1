rule BaseRecalibrator:
    input:
         a=rules.MarkDuplicate.output.MarkDuplicateBam,
         b=rules.MarkDuplicate.output.MarkDuplicateBamIndex
         
    output:
         temp(f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/qual/{{sample}}.sort.markdup.recal_data.table")
    conda: "../envs/quack.yml"
    threads: config["threads_calling"] 
    log: (f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/log/calling/{{sample}}.BaseRecalibrator.log")
    shell:
         '''
         gatk BaseRecalibrator -I {input.a} -O {output} -R {REF} {KNOWN_SITES} 2> {log}
         '''
    
rule ApplyBQSR:
    input:
         mdbam=rules.MarkDuplicate.output.MarkDuplicateBam,
         mdbamindex=rules.MarkDuplicate.output.MarkDuplicateBamIndex,
         recaltable=rules.BaseRecalibrator.output
         
    output:
         bamBQSR=temp(f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/tmp/{{sample}}.sort.markdup.recal.bam"),
         bamBQSRindex=temp(f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/tmp/{{sample}}.sort.markdup.recal.bai"),
         sbamBQSR=f"{BAM_DIR}/{PROJECT_NAME}/{{sample}}/{{sample}}.markdup.recal.sort.bam"
    conda: "../envs/quack.yml"
    threads: config["threads_calling"]
    log: (f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/log/calling/{{sample}}.ApplyBQSR.log")          
    shell:
         '''
         gatk ApplyBQSR -R {REF} -I {input.mdbam} --bqsr-recal-file {input.recaltable} -O {output.bamBQSR} --static-quantized-quals 10 --static-quantized-quals 20 --static-quantized-quals 30 2> {log}
         samtools sort -T {WORKING_DIR}/{PROJECT_NAME}/{wildcards.sample}/tmp/ -o {output.sbamBQSR} {output.bamBQSR} 2>> {log}
         samtools index {output.sbamBQSR}  2>> {log}
         '''
         
rule HaplotypeCaller:
    input:
         rules.ApplyBQSR.output.sbamBQSR
         
    output:
         gvcf=f"{GVCF_DIR}/{PROJECT_NAME}/{{sample}}/{{sample}}.g.vcf"
    conda: "../envs/quack.yml"
    threads: config["threads_calling"]          
    log: (f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/log/calling/{{sample}}.HaplotypeCaller.log")
    shell:
         '''
         gatk HaplotypeCaller -R {REF} -I {input} --emit-ref-confidence GVCF -L {TARGET_DIR}/{TARGET_SET}.extended.bed -O {GVCF_DIR}/{PROJECT_NAME}/{wildcards.sample}/{wildcards.sample}.g.vcf 2> {log}
         '''
