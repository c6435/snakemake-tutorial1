##QUALITY
#Performing fastp analysis on fastQ files

rule FastpQuality:
    input:
         r1=get_fastq_R1,
         r2=get_fastq_R2
    output:
         r1trim= f"{FASTQ_DIR}/{{sample}}/{{unit}}_R1_fastp-trim.fastq.gz",
         r2trim= f"{FASTQ_DIR}/{{sample}}/{{unit}}_R2_fastp-trim.fastq.gz"
    log: f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/log/quality/{{unit}}_fastp.log"
    conda: "../envs/fastp.yml"
    threads: config["threads_quality"]
    shell:
         '''
         fastp -i {input.r1} -I {input.r2} -o {output.r1trim} -O {output.r2trim} -j {FASTQ_DIR}/{wildcards.sample}/{wildcards.unit}.json -h {FASTQ_DIR}/{wildcards.sample}/{wildcards.unit}.html --dont_overwrite 2> {log}
         '''