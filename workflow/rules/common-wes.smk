##### Import config file var #####
REF=config["ref"]
TARGET_DIR=config["target_dir"]
TARGET_SET=config["target_set"]
KNOWN_SITES=[ "--known-sites " + x for x in config["known_sites"].split(",")]
WORKING_DIR=config["working_dir"]
PROJECT_NAME=config["project_name"]
FASTQ_DIR=config["fastq_dir"]
BAM_DIR=config["bam_dir"]
GVCF_DIR=config["gvcf_dir"]
REFSEQ_GENES=config["refseq_genes"]

##### Import python modules ##### 
import peppy
import os
import pandas as pd
import sys

##### Get running datetime  ##### 
from datetime import datetime
now = datetime.now()
date=[now.strftime("%Y-%m-%dT%H:%M:%S+0200"),now.strftime("%b.%d.%Y-%H.%M.%S")]

##### Extract configfile path from command line and import peppy project ##### 
args = sys.argv
config_path = args[args.index("--configfiles") + 1]
p = peppy.Project(config_path)
    

x=(p.sample_table.set_index('sample_name')
              .apply(lambda x: x.apply(pd.Series).stack())
              .reset_index()
              .drop('level_1', 1))
              
samples = x.set_index(["sample_name"], drop=False)            
units = x.set_index(["sample_name", "ReadGroup"], drop=False)
units.index = units.index.set_levels(
    [i.astype(str) for i in units.index.levels]
)  # enforce str in index    
    
    
##### Wildcard constraints #####
wildcard_constraints:
    sample="|".join(samples.index),
    unit="|".join(units["ReadGroup"]),

    
def get_fastq_R1(wildcards):
    """Get read1 files of given sample-unit."""
    fastq_R1 = units.loc[(wildcards.sample, wildcards.unit), ["read1"]].dropna()
    path_fastq_R1=FASTQ_DIR+"/"+fastq_R1
    return path_fastq_R1
    
def get_fastq_R2(wildcards):
    """Get read2 files of given sample-unit."""
    fastq_R2 = units.loc[(wildcards.sample, wildcards.unit), ["read2"]].dropna()
    path_fastq_R2=FASTQ_DIR+"/"+fastq_R2
    return path_fastq_R2

def get_read_group(wildcards):
    """Denote sample name and platform in read group."""
    return r"-R '@RG\tID:{sample}_{lane}_{flowcell}\tSM:{sample}\tDT:{dat}\tLB:{library}\tPL:{platform}\tCN:{provider}'".format(
        sample=wildcards.sample,
        lane=units.loc[(wildcards.sample, wildcards.unit), "Lane"],
        flowcell=units.loc[(wildcards.sample, wildcards.unit), "FlowCell"],
        dat=date[0],
        library=units.loc[(wildcards.sample, wildcards.unit), "Library"],
        platform=units.loc[(wildcards.sample, wildcards.unit), "Platform"],
        provider=units.loc[(wildcards.sample, wildcards.unit), "Provider"],
    )
    
def get_sample_bams(wildcards):
    """Get bam files of given sample."""
    bams = WORKING_DIR+"/"+PROJECT_NAME+"/"+wildcards.sample+"/tmp/"+samples.loc[wildcards.sample].ReadGroup+".fixmate.bam"
    return bams