rule BwaMap:
    input:
         r1=rules.FastpQuality.output.r1trim,
         r2=rules.FastpQuality.output.r2trim
         
    output:
         sbam=temp(f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/tmp/{{unit}}.sort.bam"),
         fsbam=temp(f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/tmp/{{unit}}.fixmate.bam")
    threads: config["threads_alignment"]
    params:
         bwa_mem_opt="-M",
         readgroup= get_read_group,
         samtools_thread=int(config["threads_alignment"]/2)
    message: '''
         Creating SAM file {wildcards.unit}.sam by aligning reads from fastQ files to reference genome {REF} using bwa (threads={threads}).
         Converting SAM file {wildcards.unit}.sam into BAM file {wildcards.unit}.bam, sorting and indexing BAM file {wildcards.unit}.sort.bam
         ReadGroup={wildcards.unit}  Sample={wildcards.sample}
         '''
    log: (f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/log/alignment/{{unit}}.BwaMap.log")
    conda: "../envs/quack.yml"
    shell: 
         '''
         bwa mem {params.bwa_mem_opt} -t {threads} {params.readgroup} {REF} {input.r1} {input.r2} | samtools sort - -m 4G -n -@ {params.samtools_thread} -T  {WORKING_DIR}/{PROJECT_NAME}/{wildcards.sample}/tmp/ -o {output.sbam} 2> {log}
         samtools fixmate -m -@ {threads} {output.sbam} {output.fsbam} 2>> {log}
         '''
        
          
rule MergeBam:
    input:
         idbam=get_sample_bams
         
    output:
         mbam=temp(f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/tmp/{{sample}}.fixmate.bam"),
         smbam=temp(f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/tmp/{{sample}}.sort.fixmate.bam"),
         smbamindex=temp(f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/tmp/{{sample}}.sort.fixmate.bam.bai")
    threads: config["threads_alignment"]     
    log: (f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/log/alignment/{{sample}}.MergeBam.log")
    
    conda: "../envs/quack.yml"
    
    shell:
     """
     input_idbam=({input.idbam})
     if [ "${{#input_idbam[@]}}" -gt 1 ]; then
         samtools merge {output.mbam} {input.idbam} 2> {log}
         samtools sort -@ {threads} -T {WORKING_DIR}/{PROJECT_NAME}/{wildcards.sample}/tmp/ -o {output.smbam} {output.mbam} 2>> {log}
         samtools index {output.smbam} 2>> {log}   ## MergeBam command for 2 input files
     elif [ "${{#input_idbam[@]}}" -eq 1 ]; then
          mv {input.idbam} {output.mbam}
          samtools sort -@ {threads} -T {WORKING_DIR}/{PROJECT_NAME}/{wildcards.sample}/tmp/ -o {output.smbam} {output.mbam} 2> {log}
          samtools index {output.smbam} 2>> {log}   ## MergeBam command for 1 input file
     else
         echo "No BAM file. Abort!" ## error handling
     fi
     """
       

rule MarkDuplicate:
    input:
         rules.MergeBam.output.smbam
         
    output:
         MarkDuplicateBam=temp(f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/tmp/{{sample}}.sort.markdup.bam"),
         MarkDuplicateBamIndex=temp(f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/tmp/{{sample}}.sort.markdup.bam.bai"),
         stats=(f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/qual/{{sample}}.markdup.stats")
         
    log: (f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/log/alignment/{{sample}}.MarkDuplicate.log")
    conda: "../envs/quack.yml"
    threads: config["threads_alignment"]  
    shell:
         '''
         samtools markdup -s -@ {threads} {input} {output.MarkDuplicateBam} 2> {output.stats} 1> {log}
         samtools index {output.MarkDuplicateBam} 2>> {log}
         '''

rule FlagStatIdxstats:
    input:
         a=rules.MarkDuplicate.output.MarkDuplicateBam,
         b=rules.MarkDuplicate.output.MarkDuplicateBamIndex
    output:
         flagstat=f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/qual/{{sample}}.sort.markdup.flagstat",
         idxstats=f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/qual/{{sample}}.sort.markdup.idxstats"
         
    log: (f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/log/alignment/{{sample}}.FlagStat-Idxstats.log")
    conda: "../envs/quack.yml"
    
    threads: config["threads_alignment"] 
    
    shell:
         '''
         samtools flagstat {input.a} > {output.flagstat} 2> {log}
         samtools idxstats {input.a} > {output.idxstats} 2>> {log}
         '''

rule ValidateSamFile:
    input:
         a=rules.MarkDuplicate.output.MarkDuplicateBam,
         b=rules.MarkDuplicate.output.MarkDuplicateBamIndex
    output:
         f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/qual/{{sample}}.samtools.quickcheck.txt",
         f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/qual/{{sample}}.sort.markdup.validate"
         
    log: (f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/log/alignment/{{sample}}.ValidateSamFile.log")
    conda: "../envs/quack.yml"
    shell:
         '''
         samtools quickcheck -vvvv {input.a} &>> {WORKING_DIR}/{PROJECT_NAME}/{wildcards.sample}/qual/{wildcards.sample}.samtools.quickcheck.txt
         picard ValidateSamFile I={input.a} IGNORE=INVALID_VERSION_NUMBER IGNORE=MATE_CIGAR_STRING_INVALID_PRESENCE MODE=SUMMARY > {WORKING_DIR}/{PROJECT_NAME}/{wildcards.sample}/qual/{wildcards.sample}.sort.markdup.validate  2> {log}        
         '''  