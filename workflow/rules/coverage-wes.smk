rule DepthOfCoverage:
    input:
         rules.ApplyBQSR.output.sbamBQSR
         
    output:
         f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/qual/{{sample}}.coverage.sample_gene_summary"
    conda: "../envs/gatk3.yml"
    params:
         f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/qual/{{sample}}.coverage"
    threads: config["threads_coverage"] 
    log: (f"{WORKING_DIR}/{PROJECT_NAME}/{{sample}}/log/coverage/coverage.{{sample}}.DepthOfCoverage.log")
    shell:
         '''
         java -Xmx8G -jar /opt/miniconda2/envs/gatk3/opt/gatk-3.8/GenomeAnalysisTK.jar -T DepthOfCoverage -R {REF} -I {input} -L {TARGET_DIR}/{TARGET_SET}.bed -geneList {REFSEQ_GENES} -o {params} -omitBaseOutput -ct 20 2> {log}
         '''
