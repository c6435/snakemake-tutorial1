### Accedere allo spazio di lavoro su GitPod
Utilizzando il link aprite nel vostro browser lo spazio di lavoro predefinito di [GitPod](https://gitpod.io/#https://gitlab.com/c6435/snakemake-tutorial1) contenente il workflow per l'analisi dell'esoma.

<details><summary>Dataset</summary>

```
Il nostro set di dati di partenza consiste in letture FASTQ (Illumina, 100bp paired-end) di un 
soggetto affetto da osteopetrosi. Le regioni target sono state acquisite con il kit Nextera Rapid 
Capture Expanded Exome (62MB). Per ridurre il tempo di calcolo, abbiamo preselezionato le letture 
che mappano principalmente sul cromosoma 8.
```

</details>


### Requisiti
Per eseguire l'analisi è necessario creare 3 files:


1. Il flusso di lavoro exome-germline-SNV-indels-smk richiede che i campioni siano elencati in un file 
   csv (modello di riferimento `config/sample.model.csv`).

   Crea un nuovo file copiando il file modello e sostituisci le sezioni indicate tra `<>` con i dati relativi al campione `proband` presente nella cartella `/data/samples`.
   
   **NB.** Attribuire al file l'estensione `.csv`. Le colonne read1, read2 e ReadGroup del file fanno riferimento alla sezione `sources:` del file YAML di configurazione (`workflow/config/config.model.yaml`) in cui è possibile indicare lo schema relativo al nome dei fastq file. 


2. Per ciascun campione è necessario indicare in un file csv di testo i **metadati** relativi alla corsa di sequenziamento:
   - **Sample:** Sample Name
   - **FlowCell:** FlowCell ID
   - **Lane:** FlowCell Lane number
   - **Index:** Index Sequence
   - **Enrichment:** Target enrichment used
   - **Library:**  library performed (Valid value: PE for paired-end and SE for single-end)
   - **Platform:** Sequencing platform used (Valid value: Illumina, SOLID, LS454, HELICOS e PACBIO)
   - **Provider:** Name of the sequencing provider <br>
   <br>

   
   Nel nostro caso l'intestazione dei files FASTQ utilizzati é:

   ```
   @DCW97JN1:309:C0C42ACXX:2:1209:3240:6385/1
   ```

   dove

   ``` 
   @DCW97JN1 : nome dello strumento 
   309 : id della run 
   C0C42ACXX : id della flowcell
   2 : id della lane
   1209 : numero della tile all'interno della lane della flowcell 
   3240 : coordinata x del cluster all'interno della tile 
   6385 : coordinata y del cluster all'interno della tile 
   /1 : membro della coppia, /1 o /2 (soltanto per le read accoppiate) 
   ```

   Crea un nuovo file copiando il file modello (`workflow/config/sample-meta.model.csv`) e sostituisci le 
   sezioni indicate tra `<>` con i metadati relativi al campione.
   
   **NB.** Attribuire al file l'estensione `.csv`.

3. Infine è necessario definire un file YAML di configurazione contenente le informazioni relative ai 
   file di supporto necessari per l'esecuzione del workflow.
   I file csv creati negli step precedenti vanno indicati nel file YAML di configurazione nella sezione: 
   ```
   sample_table: <path/to/sample-csv>
   subsample_table: <path/to/sample-meta-csv>
   ```
   Crea un nuovo file copiando il file modello (`workflow/config/config.model.yaml`) e sostituisci le sezioni indicate tra `<>` con le informazioni richieste.
   
   **NB.** Attribuire al file l'estensione `.yaml`.
   Il file YAML fornito è un modello YAML di esempio del flusso di lavoro pronto all'uso. È 
   responsabilità dell'utente impostare correttamente le variabili di input di riferimento.


### Come eseguire il workflow

Da terminale attivare l'ambiente conda contenente snakemake:
```
conda activate snakemake
```
Spostarsi nella cartella contenente lo Snakefile:

```
cd /workspace/snakemake-tutorial1/workflow
```

Verificare che la dryrun del workflow venga eseguita con successo:

```
snakemake --configfiles </path/to/config-yaml> -np
```

Lanciare il workflow utilizzando il comando:

```
snakemake --configfiles </path/to/config-yaml> --use-conda --cores all
```
